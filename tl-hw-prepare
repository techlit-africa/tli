#!/bin/bash

# Fail on any bad status code
set -e

usage="
$(basename $0) usage:
====

  $(basename $0) <system disk> <mount directory> [<recovery iso>] [<update archive>]

arguments:
====

  <mount directory>
    directory used to mount & configure the root partition

  <system disk>
    hard drive where the OS will live that is a writable block device
    it will be completely destroyed! (But not securely erased.)
    and will then have five partitions:
      mbr as 1, efi as 2, recovery iso as 3, root as 4 and data as 5.

examples:
====

  $(basename $0) /dev/sdx /mnt
"

# If the user is clearly asking for help, exit here
if [[ "$1" =~ "help" ]] || [[ "$1" =~ "h$" ]]; then
	echo -e "$usage"
	exit 1
fi

# Name script arguments
disk="$1"
mnt="$2"

# Collect issues with arguments in $msg variable
msg=""
add() { msg="$msg\n  $1"; }

if [[ -z "$disk" ]]; then
	add "Not sure which disk to use (the first argument is missing or blank)"
else
	[[ -b "$disk" ]] || add "disk must be a block device"
	[[ -w "$disk" ]] || add "disk must writable"
fi

if [[ -z "$mnt" ]]; then
	add "Not sure which mountpoint to use (the second argument is missing or blank)"
else
	[[ -d "$mnt" ]] || add "Mountpoint must be a directory"
fi

# If there are any issues with the arguments, fail here
if ! [[ -z "$msg" ]]; then
	echo -e "One or more arguments are invalid:\n$msg"
	echo -e "$usage"
	exit 1
fi

echo "Settings:"
echo "  disk=$disk"
echo "  mnt=$mnt"
echo

run() { echo -e "  $ $@"; $@; }

echo "Partitioning and labelling..."
run parted "$disk" -s mktable gpt
run parted "$disk" -s mkpart primary 0 10M
run parted "$disk" -s mkpart primary fat32 10M 522M
run parted "$disk" -s mkpart primary 522M 1546M
run parted "$disk" -s mkpart primary ext4 1546M 60G
run parted "$disk" -s mkpart primary ext4 60G 100%
run parted "$disk" -s set 1 bios_grub on
run parted "$disk" -s set 2 esp on

efi=$(tl-util-part-name $disk 2)
root=$(tl-util-part-name $disk 4)
data=$(tl-util-part-name $disk 5)

part_name=$(echo $efi | sed -e 's;/dev/;;g')
while ! lsblk | grep "$part_name" > /dev/null; do
	echo 'Waiting for kernel to recognize new partitions...'
	run partprobe "$disk"
	run sleep 1.5
done

echo "Creating filesystems..."
run mkfs.fat -F32 $efi
run mke2fs -t ext4 -F $root
run mke2fs -t ext4 -F $data

for ext4fs in $root $data; do
  run mount "$ext4fs" "$mnt"
  [[ -d "${mnt}/lost+found" ]] && rmdir "${mnt}/lost+found"
  umount -R "$mnt"
done

echo "Done. Machine is ready for use."
